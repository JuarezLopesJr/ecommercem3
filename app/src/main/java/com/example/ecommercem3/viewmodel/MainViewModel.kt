package com.example.ecommercem3.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ecommercem3.core.data.local.model.Deliverer
import dagger.hilt.android.lifecycle.HiltViewModel
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.notifications.InitialResults
import io.realm.kotlin.notifications.ResultsChange
import io.realm.kotlin.notifications.UpdatedResults
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

@HiltViewModel
class MainViewModel @Inject constructor(
    private val realm: Realm
) : ViewModel() {

    fun getDelivererNameById(delivererId: String): Flow<ResultsChange<Deliverer>> {
        val nameFlow =
            realm.query<Deliverer>("deliverer_id = $0", delivererId).asFlow()

        viewModelScope.launch(Dispatchers.Main) {
            nameFlow.collect { results ->
                when (results) {
                    is InitialResults -> {
                        for (name in results.list) {
                            Log.d("deliverer_name: ", name.toString())
                        }
                    }
                    is UpdatedResults -> TODO()
                }
            }
        }

        return nameFlow
    }
}