package com.example.ecommercem3.core.data.local.model

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.RealmUUID
import io.realm.kotlin.types.annotations.PrimaryKey

class Product() : RealmObject {
    @PrimaryKey
    val productId: String = RealmUUID.random().toString()
    val pricePerAmount: Float = 0.0f
    val belongsToDeliverer: String = ""
}