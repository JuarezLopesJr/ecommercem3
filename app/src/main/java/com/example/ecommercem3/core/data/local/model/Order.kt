package com.example.ecommercem3.core.data.local.model

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.RealmUUID
import io.realm.kotlin.types.annotations.PrimaryKey

class Order() : RealmObject {
    @PrimaryKey
    val orderId: String = RealmUUID.random().toString()
    val date: String = ""
    val delivererTime: String = ""
    val delivererName: String = ""
}