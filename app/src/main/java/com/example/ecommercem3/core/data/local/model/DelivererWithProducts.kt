package com.example.ecommercem3.core.data.local.model

import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject

class DelivererWithProducts() : RealmObject {
    val deliverer: Deliverer? = null
    val products: RealmList<Product> = realmListOf()
}