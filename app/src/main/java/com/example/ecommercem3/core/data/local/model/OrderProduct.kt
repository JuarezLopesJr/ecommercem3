package com.example.ecommercem3.core.data.local.model

import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.RealmUUID
import io.realm.kotlin.types.annotations.PrimaryKey

class OrderProduct() : RealmObject {
    @PrimaryKey
    val _id: ObjectId = ObjectId.create()
    val orderId: String = RealmUUID.random().toString()
    val productId: String = RealmUUID.random().toString()
    val amount: Int = 0
}