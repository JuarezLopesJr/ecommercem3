package com.example.ecommercem3.di

import com.example.ecommercem3.core.data.local.model.Deliverer
import com.example.ecommercem3.core.data.local.model.Order
import com.example.ecommercem3.core.data.local.model.OrderProduct
import com.example.ecommercem3.core.data.local.model.Product
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RealmModule {
    @Provides
    @Singleton
    fun provideRealmDatabase(): Realm {
        val realmConfiguration = RealmConfiguration
            .Builder(
                schema = setOf(
                    Deliverer::class,
                    Order::class,
                    OrderProduct::class,
                    Product::class
                )
            )
            .schemaVersion(1)
            .build()

        return Realm.open(realmConfiguration)
    }
}